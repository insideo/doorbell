#include <FS.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <WiFiManager.h>
#include <Ticker.h>
#include <ArduinoJson.h>

#ifndef LED_BUILTIN
#define LED_BUILTIN 13
#endif

#define PIN_CONFIG D7
#define PIN_DOORBELL D6
#define PIN_LED D5

Ticker ticker;
WiFiManager wifiManager;
char callback_url[256] = "";
char heartbeat_url[256] = "";
bool shouldSaveConfig = false;
volatile uint32_t cycleCounter = 0;

int32_t cooldown = 0;
uint32_t loopCount = 0;
uint32_t errorCount = 0;

void tick() {
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
}

void saveConfigCallback() {
  Serial.println("Config save requested");
  shouldSaveConfig = true;
}

void configModeCallback(WiFiManager *wfm) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  Serial.println(wfm->getConfigPortalSSID());
  ticker.attach(0.2, tick);
}

void IRAM_ATTR doorbellInterrupt() {
  cycleCounter++;
}

bool sendNotification() {
  WiFiClient client;
  HTTPClient http;

  Serial.print("Connecting to ");
  Serial.println(callback_url);

  if (http.begin(client, callback_url)) {
    http.addHeader("Accept", "application/json");

    int httpCode = http.POST("");
    http.end();

    Serial.print("Response code: ");
    Serial.println(httpCode);

    return (httpCode >= 200 && httpCode <= 299);    
  } else {
    Serial.println("Unable to send notification");
    return false;
  }
}

bool sendHeartbeat() {
  WiFiClient client;
  HTTPClient http;
  http.setTimeout(3000);

  Serial.print("Connecting to ");
  Serial.println(heartbeat_url);

  if (http.begin(client, heartbeat_url)) {
    http.addHeader("Accept", "application/json");

    int httpCode = http.POST("");
    http.end();

    Serial.print("Response code: ");
    Serial.println(httpCode);

    return (httpCode >= 200 && httpCode <= 299);    
  } else {
    Serial.println("Unable to send heartbeat");
    return false;
  }
}

void forceConfig() {
  if (SPIFFS.begin()) {
    File file = SPIFFS.open("/config.force", "w");
    if (file) {
      file.write("1");
      file.close();
      Serial.println("Wrote /config.force");
    } else {
      Serial.println("Unable to write /config.force");
    }
    SPIFFS.end();
  } else {
    Serial.println("Unable to open SPIFFS filesystem");
  }
}

void unforceConfig() {
  if (SPIFFS.begin()) {
    if (SPIFFS.exists("/config.force")) {
      if (SPIFFS.remove("/config.force")) {
        Serial.println("Removed /config.force");
      } else {
        Serial.println("Unable to remove /config.force");
      }
    }
    SPIFFS.end();
  } else {
    Serial.println("Unable to open SPIFFS filesystem");
  }
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PIN_CONFIG, INPUT_PULLUP);
  pinMode(PIN_DOORBELL, INPUT);
  pinMode(PIN_LED, OUTPUT);
  digitalWrite(PIN_LED, LOW);

  ticker.attach(0.6, tick);

  Serial.begin(115200);
  Serial.println();
  delay(3000);
  Serial.println();
  
  bool conf = false;

  // read configuration
  Serial.println("Mounting filesystem");
  if (SPIFFS.begin()) {
    Serial.println("Mounted filesystem");

    if (SPIFFS.exists("/config.json")) {
      Serial.println("Reading configuration file /config.json");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("Opened config file");
        size_t size = configFile.size();
        std::unique_ptr<char[]> buf(new char[size]);
        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        json.printTo(Serial);
        Serial.println();
        if (json.success()) {          
          Serial.println("Parsed json");
          strcpy(callback_url, json["callback_url"]);
          strcpy(heartbeat_url, json["heartbeat_url"]);
          conf = true;
        } else {
          Serial.println("Failed to load json config");
        }
      }   
    } else {
      Serial.println("Configuration file /config.json not found");
    }
    if (SPIFFS.exists("/config.force")) {
      Serial.println("Configuration forced by user.");
      conf = false;
    }
    SPIFFS.end();
  } else {
    Serial.println("Failed to mount filesystem");
  }

  WiFiManagerParameter custom_callback_url("callback_url", "Callback URL", callback_url, 256);
  WiFiManagerParameter custom_heartbeat_url("heartbeat_url", "Heartbeat URL", heartbeat_url, 256);
  wifiManager.addParameter(&custom_callback_url);
  wifiManager.addParameter(&custom_heartbeat_url);
  wifiManager.setAPCallback(configModeCallback);
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  if (!conf) {
    Serial.println("Configuration required");
    unforceConfig();
    if (!wifiManager.startConfigPortal("Doorbell Setup")) {
      Serial.println("WiFi configuration failed. Rebooting...");
      delay(3000);
      ESP.restart();
      delay(5000);
    }
  } else if (!wifiManager.autoConnect("Doorbell Setup")) {
    Serial.println("WiFi connection failed. Rebooting...");
    delay(3000);
    ESP.restart();
    delay(5000);
  }

  if (shouldSaveConfig) {
    if (SPIFFS.begin()) {
      strcpy(callback_url, custom_callback_url.getValue());
      strcpy(heartbeat_url, custom_heartbeat_url.getValue());

      Serial.println("Saving configuration /config.json");
      DynamicJsonBuffer jsonBuffer;
      JsonObject& json = jsonBuffer.createObject();
      json["callback_url"] = callback_url;
      json["heartbeat_url"] = heartbeat_url;

      File configFile = SPIFFS.open("/config.json", "w");
      if (!configFile) {
        Serial.println("Failed to open config file for writing");
      }

      json.printTo(Serial);
      json.printTo(configFile);
      configFile.close();
      Serial.println();
      Serial.println("Saved configuration");
    } else {
      Serial.println("Unable to open SPIFFS filesystem");
    }
    unforceConfig();
  }

  Serial.println("WiFi connected.");
  Serial.println();

  Serial.print("         SSID: ");
  Serial.println(WiFi.SSID());
  Serial.print("  MAC Address: ");
  Serial.println(WiFi.macAddress());
  Serial.print("   IP Address: ");
  Serial.println(WiFi.localIP());
  Serial.print("      Gateway: ");
  Serial.println(WiFi.gatewayIP());
  Serial.print(" Callback URL: ");
  Serial.println(callback_url);
  Serial.print("Heartbeat URL: ");
  Serial.println(heartbeat_url);
  Serial.println();

  attachInterrupt(digitalPinToInterrupt(PIN_DOORBELL), doorbellInterrupt, RISING);

  ticker.detach();
  digitalWrite(LED_BUILTIN, HIGH);
}

void loop() {
  bool connected = WiFi.isConnected();

  if (digitalRead(PIN_CONFIG) == LOW) {
    Serial.println("Configuration reset requested by user, rebooting...");
    digitalWrite(LED_BUILTIN, HIGH);
    wifiManager.resetSettings();
    forceConfig();
    delay(3000);
    ESP.reset();
    delay(5000);
  } else if (!connected) {
    Serial.println("WiFi connection failed, rebooting...");
    delay(3000);
    ESP.reset();
    delay(5000);
  }  

  cycleCounter = 0;
  delay(100);
  uint32_t count = cycleCounter;


  // some activity every 10 seconds
  if (loopCount % 100 == 0) {
    Serial.println("Heartbeat...");
    digitalWrite(LED_BUILTIN, LOW);
    if (sendHeartbeat()) {
      Serial.println("Heartbeat: OK");
      errorCount = 0;
    } else {
      Serial.println("Heartbeat: ERR");
      errorCount++;
      if (errorCount >= 6) {
        Serial.println("Heartbeat failure for > 1 min, rebooting...");
        delay(3000);
        ESP.reset();
        delay(5000);
      }
    }
  }

  if (loopCount % 100 == 1) {
    digitalWrite(LED_BUILTIN, HIGH);
  }
  loopCount++;

  // if cycles at 2/3 duty or more, ring the chime
  if (count >= 4) {
    // but only if it hasn't gone off in the last 5 seconds
    if (cooldown <= 0) {
      Serial.print("Doorbell detected, cycle count: ");
      Serial.println(count);
      digitalWrite(PIN_LED, HIGH);
      if (!sendNotification()) {
        Serial.println("Sending notification failed");
      }
      cooldown = 50;
    } else {
      Serial.println("Cooldown in progress, skipping notification");
    }
  } else if (count > 0) {
    Serial.print("Transient doorbell signal, cycle count: ");
    Serial.println(count);
  }

  if (cooldown > 0) {
      digitalWrite(PIN_LED, cooldown >= 40 ? HIGH : LOW);
    cooldown--;
  }

}
